'use strict'

var app = angular.module('chirpApp', []);

app.controller('mainController', ($scope) => {
    $scope.posts = [];
    $scope.newPost = { created_by : '', create_at : '' };

    $scope.post = () => {
        if(($scope.newPost.created_by == undefined) ||  ($scope.newPost.text == undefined)) {
            location.reload();
            return;
        }

        $scope.newPost.create_at = Date.now();
        $scope.posts.push($scope.newPost);
        $scope.newPost = { created_by : '', create_at : '' };
    }
});

app.controller('authController', ($scope) => {
    $scope.user = { userName : '', userPassword : '' };
    $scope.error_message = '';

    $scope.login = () => {
        //Placeholder until authentication is implemented
        $scope.error_message = 'Login request for ' + $scope.user.userName;
    }

    $scope.register = () => {
        //Placeholder until authentication is implemented
        $scope.error_message = 'Registration request for ' + $scope.user.userName;
    }

});