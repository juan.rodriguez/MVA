var express = require('express');
var multer = require('multer');

var router = express.Router();

/* GET home page. */
router.route('/posts')
    //Return all post
    .get((req, res, next)=> {
        console.log(req.query);
        res.status(200).render('index', { title : 'Chirp App' });
    })
    
    //Retunr post petition only
    .post((req, res, next) => {
        console.log(req.body);
        res.status(200).send({ Message : 'TODO Create a new post.' });
    });

/* RETURN A PARTICULAR POST */
router.route('/posts/:id')
    .post((req, res, next) => {
        console.log(req.query);
        res.status(200).send({ Message : 'TODO return post with ID ' + req.params.id });
    });

module.exports = router;